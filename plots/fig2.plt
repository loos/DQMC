set term pdf enhanced font "Times,18" rounded linewidth 2.2
set key bottom right
set format y "%.4f"
set format x "%.3f"
set grid

# set term X11 enhanced
set xlabel "E"
set ylabel "ℰ(E)"

set label 1 "E_0" at -0.793, -0.7678,  0 left norotate
set label 2 "E_0" at -0.7675, -0.7657,  0 left norotate

#set term x11
set output "fig2.pdf"
set xrange[-0.8:-0.76]
set yrange[-0.7685:-0.765]
set arrow from -0.768068,-0.7685 to -0.768068,-0.765 nohead ls 3
plot \
"two_compo_fig2" u 1:2:3 notitle w l ls 2, \
"data_fig2_accurate" u 1:2:3 notitle  w p ls 1, \
-0.768068 notitle, \

